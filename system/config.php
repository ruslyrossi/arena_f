<?php
// Show Error for debugging
error_reporting(E_ALL); 
ini_set('display_errors', 1);

// Start session $_SESSION
session_start();

// Set default timezone
date_default_timezone_set("Asia/Kuala_Lumpur");

// Config CONSTANT
define('DB_HOSTNAME', '127.0.0.1');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'arena88');
define('SALT', '987787654565398765678'); // 20 digit
define('SITE_URL', 'http://localhost/arena_f/'); // 20 digit
define('PAGINATION_MAX_PER_PAGE', '10');
define('ADMIN_EMAIL', 'ruslyrossi47@gmail.com');

// Connect to Database; MySQLi with OOP format : 
// refer: http://stackoverflow.com/questions/552336/oop-vs-functional-programming-vs-procedural
$db = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);  

// Catch Error if connection failed
if ($db->connect_errno) {
	die('Sorry, we are having some problems.');
}

// Load default function
include('function.php');

?>