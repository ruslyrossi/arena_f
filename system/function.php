<?php
function is_login() {
		
	if($_SESSION['front_login'] == 0 ) {
		header('Location: '.SITE_URL.'login.php?status=logout');
	}
}

function logout() {
	
	unset($_SESSION['front_login']);
	header('Location: '.SITE_URL.'login.php?status=logout');
}

function escape($str) {
	
	global $db;
	
	return $db->real_escape_string($str);	
}

function debug($array) {
	
	echo '<pre>', print_r($array), '</pre>'; 
}

function excerpt($text, $number_of_words) {
   // Where excerpts are concerned, HTML tends to behave
   // like the proverbial ogre in the china shop, so best to strip that
   $text = strip_tags($text);

   // \w[\w'-]* allows for any word character (a-zA-Z0-9_) and also contractions
   // and hyphenated words like 'range-finder' or "it's"
   // the /s flags means that . matches \n, so this can match multiple lines
   $text = preg_replace("/^\W*((\w[\w'-]*\b\W*){1,$number_of_words}).*/ms", '\\1', $text);

   // strip out newline characters from our excerpt
   return str_replace("\n", "", $text);
}

?>







