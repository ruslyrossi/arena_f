<!DOCTYPE html>
<html>
   <head>
    <title>Arena 88</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale1">
    
    <link rel="stylesheet" href="css/style.css" media="all">
    <link rel="stylesheet" href="css/bootstrap.css" media="all">
    <link rel="stylesheet" href="css/responsive.css" media="all">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"><!-- API http://fortawesome.github.io/Font-Awesome/ -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/bootstrap.min.js"></script>
   </head>
   <body>
   
   		<div class="header">
                <div class="col-xs-10 col-xs-offset-1">
                
                	<div id="logo">
                    	<img src="images/logo.png">
                    </div>
                    
                    <div class="member pull-right">
                    	<a href="#">Member</a>
                    </div>
                    <div id="menu" class="pull-right">
                    	<ul class="nav nav-pills">
                          <li class="active"><a href="#">Home</a></li>
                          <li><a href="#">About</a></li>
                          <li><a href="#">Announcement</a></li>
                          <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    
                </div>
              <div class="clearfix"></div>  
    	</div>
        
        
        <div id="content"><!--content-->
            <!-- Features -->
            <div class="page">
            
              <div class="row container center-block">
                <div class="col-md-12">
                  <h1>Announcement</h1>
                </div>
              </div>

                  <div class="row container center-block">
                    
                    <div class="col-md-12 post">
                      <p><b>Nullam consequat porta sem, sed vestibulum mauris commodo consequat</b></p>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt augue ligula, in blandit arcu auctor vitae. Proin eleifend dolor dui, a dignissim mauris rutrum nec. Sed pulvinar lorem sit amet cursus auctor. Nullam consequat porta sem, sed vestibulum mauris commodo consequat. Nulla facilisis vitae elit id cursus. Donec dignissim malesuada pellentesque. </p>
                    
                      <button class="btn btn-primary btn-sm" type="button">Read More</button>
                    </div>

                    <div class="col-md-12 post">
                      <p><b>Nullam consequat porta sem, sed vestibulum mauris commodo consequat</b></p>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt augue ligula, in blandit arcu auctor vitae. Proin eleifend dolor dui, a dignissim mauris rutrum nec. Sed pulvinar lorem sit amet cursus auctor. Nullam consequat porta sem, sed vestibulum mauris commodo consequat. Nulla facilisis vitae elit id cursus. Donec dignissim malesuada pellentesque. </p>
                    
                      <button class="btn btn-primary btn-sm" type="button">Read More</button>
                    </div>

                    <div class="col-md-12 post">
                      <p><b>Nullam consequat porta sem, sed vestibulum mauris commodo consequat</b></p>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt augue ligula, in blandit arcu auctor vitae. Proin eleifend dolor dui, a dignissim mauris rutrum nec. Sed pulvinar lorem sit amet cursus auctor. Nullam consequat porta sem, sed vestibulum mauris commodo consequat. Nulla facilisis vitae elit id cursus. Donec dignissim malesuada pellentesque. </p>
                    
                      <button class="btn btn-primary btn-sm" type="button">Read More</button>
                    </div>

                    <div class="col-md-12 post">
                      <p><b>Nullam consequat porta sem, sed vestibulum mauris commodo consequat</b></p>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt augue ligula, in blandit arcu auctor vitae. Proin eleifend dolor dui, a dignissim mauris rutrum nec. Sed pulvinar lorem sit amet cursus auctor. Nullam consequat porta sem, sed vestibulum mauris commodo consequat. Nulla facilisis vitae elit id cursus. Donec dignissim malesuada pellentesque. </p>
                      
                      <button class="btn btn-primary btn-sm" type="button">Read More</button>
                    </div>
                    
                  </div>
   
                <div class="clearfix"></div>
            </div>   
            <!-- Features -->     
            
            <div class="footer">
            	<div class="row container center-block">
                   
                   <div class="footer_link">
                   	<a href="">Contact Support</a> - <a href="">About  Arena</a>
                   </div>
                   
                </div>
       
            </div>             
            
        
        </div><!--content-->
      
 
   </body>
</html>
