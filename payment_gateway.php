<?php
include('system/config.php');

is_login();

if(isset($_POST['action_pre']) == 'validate') {

    echo $_POST['action'];

    if (isset($_POST['action']) && ($_POST['action'] == 'topup')) {
     
          include('controller/membership.php');
          $status = add_cash($_POST);
            
          if($status == 1) {
            header('Location: '.SITE_URL.'account.php?status=success');
          } else {
            header('Location: '.SITE_URL.'membership.php?status=failed');
          }

    } else if(isset($_POST['action']) && ($_POST['action'] == 'add_booking_slot')) {

        include('controller/booking.php');
    
        $status = add_booking_slot($_POST);
        
        if ($status == 1) {
            header('Location: '.SITE_URL.'booking_add.php?status=success');
        } else {
            header('Location: '.SITE_URL.'booking_add.php?status=failed');
        }
    }

} else {

    // Input
    if (isset($_POST['action']) && ($_POST['action'] == 'topup')) { // membership.php

        if (isset($_POST['item'])) { 

            // Paypal info
            $item = escape($_POST['item']);
            $amount = escape($_POST['amount']);
            $action = escape($_POST['action']);

        } else {

            die('Parameter is empty!');

        }

    } else if (isset($_POST['action']) && ($_POST['action'] == 'add_booking_slot')) { // booking.php

        if (isset($_POST['item'])) { 

            // Paypal info
            $item = escape($_POST['item']);
            $amount = escape($_POST['total']);
            $action = escape($_POST['action']);

            // Booking Input
            $court_id = escape($_POST['court_id']);
            $payment_method = escape($_POST['payment_method']);
            $membership_cash = escape($_POST['membership_cash']);
            $slot = $_POST['slot'];
            $total = escape($_POST['total']);

        } else {

            die('Parameter is empty!');

        }

    }

}

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Arena 88</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale1">
        <link rel="stylesheet" href="css/style.css" media="all">
        <link rel="stylesheet" href="css/bootstrap.css" media="all">
        <link rel="stylesheet" href="css/responsive.css" media="all">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700' rel='stylesheet' type='text/css'>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/bootstrap.min.js"></script>
    </head>
    <body class="pp_bg">

        <div class="row container center-block">

            <div class="col-md-4">

                <div id="order_summary">

                    <div class="order_content">

                        <p class="center"><b>Your order summary</b></p>


                        <table class="order_details">
                            <tr>
                                <td>Description</td>
                                <td>Amount</td>
                            </tr>

                            <tr>
                                <td><span class="blue"><?php echo $item ?></span><br><br></td>
                                <td>RM<?php echo $amount ?></td>
                            </tr>
                            <tr>
                                <td><b>Item total</b></td>
                                <td><b>RM<?php echo $amount ?></b></td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td><b>RM<?php echo $amount ?></b></td>
                            </tr>
                        </table>

                    </div>

                </div>

            </div>

            <div class="col-md-8">
                
                <div class="pp_box">    
                    <div class="pp_header">
                        Pay with credit or debit card <img src="images/pp_logo.jpg" class="pull-right">
                    </div>

                    <div class="pp_form">
                    
                        <form action="payment_gateway.php" method="post">
                            <table>
                                <tr>
                                    <td align="right">Card number</td>
                                    <td><input type="text" value="" required></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><img src="images/cc.jpg"></td>
                                </tr>
                                <tr>
                                    <td align="right">Expiration date</td>
                                    <td><input type="text" value="" size="2" placeholder="mm" required> / <input type="text" value="" size="2" placeholder="yy" required></td>
                                </tr>
                                <tr>
                                    <td align="right">CSC</td>
                                    <td><input type="text" value="" size="3" required></td>
                                </tr>
                                <tr>
                                    <td align="right">Telephone</td>
                                    <td><input type="text" value="" required></td>
                                </tr>
                                <tr>
                                    <td align="right">Email</td>
                                    <td><input type="text" value="" required></td>
                                </tr>
                                 <tr>
                                    <td></td>
                                    <td><br><input type="submit" class="btn_pp" value=""></td>
                                </tr>
                            </table>
                            
                            <input type="hidden" name="item" value="<?php echo $item ?>">
                            <input type="hidden" name="amount" value="<?php echo $amount ?>">
                            <input type="hidden" name="action" value="<?php echo $action ?>">

                            <?php if (isset($_POST['action']) && ($_POST['action'] == 'add_booking_slot')) { ?>

                                <input type="hidden" name="membership_cash" value="<?php echo $membership_cash; ?>" id="membership_cash_value">
                                <input type="hidden" name="payment_method" value="<?php echo $payment_method; ?>" id="payment_method">
                                <input type="hidden" name="court_id" value="<?php echo $court_id; ?>" id="court_id">

                                <?php foreach ($slot as $item) { ?>
                                <input type="hidden" name="slot[]" value="<?php echo $item; ?>" id="slot">
                                <?php } ?>
                                <input type="hidden" name="action" value="add_booking_slot">
                                <input type="hidden" name="total" value="<?php echo $total; ?>" id="total_amount">
                                
                            <?php } ?>

                                <input type="hidden" name="action_pre" value="validate">
                            
                        </form>
     
                    </div>

                </div>

            </div>

        </div>

    </body>
</html>