<?php
include('system/config.php');
include('controller/report.php');

is_login();

if(isset($_POST['action']) == 'add_report') {
  
  // let we do some checking with our record
  $status = add_report($_POST);
    
  if($status == 1) {
    header('Location: '.SITE_URL.'account.php?status=success');
  } else {
    header('Location: '.SITE_URL.'report.php?status=failed');
  }
}
?>
<!DOCTYPE html>
<html>
   <head>
    <title>Arena 88</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale1">
    
    <link rel="stylesheet" href="css/style.css" media="all">
    <link rel="stylesheet" href="css/bootstrap.css" media="all">
    <link rel="stylesheet" href="css/responsive.css" media="all">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"><!-- API http://fortawesome.github.io/Font-Awesome/ -->
   </head>
   <body>
   
   		<div class="header">
                <div class="col-xs-10 col-xs-offset-1">
                
                	<div id="logo">
                    	<img src="images/logo.png">
                    </div>
                    
                    <div class="member pull-right">
                      <?php if (isset($_SESSION['front_login'])) { ?>
                          <a href="account.php">Account</a>
                      <?php } else { ?>
                          <a href="login.php">Member</a>
                      <?php } ?>
                    </div>
                    <div id="menu" class="pull-right">
                      <ul class="nav nav-pills">
                          <li><a href="index.php">Home</a></li>
                          <li><a href="about.php">About</a></li>
                          <li><a href="announcement.php">Announcement</a></li>
                          <li><a href="rss.php">RSS</a></li>
                          <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                    
                </div>
              <div class="clearfix"></div>  
    	</div>
        
        
        <div id="content"><!--content-->
            <!-- report -->
            <div class="page">
              
              <div class="row container center-block">

                  <?php if (isset($_GET['status']) && ($_GET['status'] == 'failed')) { ?>

                    <div class="alert alert-warning">
                      <strong>Error!</strong> Please check again your form. 
                    </div>

                  <?php } ?>
   
                  <h1>Report</h1>

                  <ol class="breadcrumb">
                    <li> <a href="account.php">My Account</a></li>
                    <li class="active">Report</li>
                  </ol>

              </div>
              
              <div class="row container center-block">
                  
                  <div class="table_header">Report Form<i class="pull-right fa fa-thumb-tack"></i></div>
                  <form role="form" class="input_form" action="report.php" method="post">
                    <div class="form-group">
                      <label for="title">Title</label>
                      <input type="text" class="form-control" id="title" name="report_title" placeholder="e.g. Court B12 damage" required>
                    </div>
                    <div class="form-group">
                      <label for="description">Description</label>
                      <textarea class="form-control" name="report_description" placeholder="Please brief some description" required></textarea>
                    </div>
                    <input type="hidden" value="add_report" name="action">
                    <button class="btn btn-primary btn-lg" type="submit">Submit</button>
                  </form>
                                    
                </div>
                
              <div class="clearfix"></div>
            </div>
            <!-- report -->     
            
            <div class="footer">
            	<div class="row container center-block">
                   
                   <div class="footer_link">
                   	<a href="">Contact Support</a> - <a href="">About  Arena</a>
                   </div>
                   
                </div>
       
            </div>             
            
        
        </div><!--content-->
      
 
   </body>
</html>

