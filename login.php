<?php
include('system/config.php');
include('controller/login.php');

/*if ($_SESSION['front_login'] == 1 ) {
    header('Location: '.SITE_URL.'account.php');
}*/

if (isset($_POST['action']) == 'front_login') {
  
  // let we do some checking with our record
  $status = login($_POST);
    
  if($status == 1) {
    header('Location: '.SITE_URL.'account.php');
  } else {
    header('Location: '.SITE_URL.'login.php?status=failed');
  }
}
?>
<!DOCTYPE html>
<html>
   <head>
    <title>Arena 88</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale1">
    
    <link rel="stylesheet" href="css/style.css" media="all">
    <link rel="stylesheet" href="css/bootstrap.css" media="all">
    <link rel="stylesheet" href="css/responsive.css" media="all">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"><!-- API http://fortawesome.github.io/Font-Awesome/ -->
   </head>
   <body>
   
   		<div class="header">
                <div class="col-xs-10 col-xs-offset-1">
                
                	<div id="logo">
                    	<img src="images/logo.png">
                    </div>
                    
                    <div class="member pull-right">
                      <a href="login.php">Member</a>
                    </div>
                    <div id="menu" class="pull-right">
                      <ul class="nav nav-pills">
                          <li><a href="index.php">Home</a></li>
                          <li><a href="about.php">About</a></li>
                          <li><a href="announcement.php">Announcement</a></li>
                          <li><a href="rss.php">RSS</a></li>
                          <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                    
                </div>
              <div class="clearfix"></div>  
    	</div>
        
        
        <div id="content"><!--content-->
            <!-- Features -->
            <div class="register">
            
            	<div class="row container center-block">


                  <?php if (isset($_GET['status']) && ($_GET['status'] == 'success')) { ?>

                    <div class="alert alert-success">
                      <strong>Well done!</strong> Your registration was successful.
                    </div>

                  <?php } ?>

                  <?php if (isset($_GET['status']) && ($_GET['status'] == 'failed')) { ?>

                    <div class="alert alert-warning">
                      <strong>Error!</strong> Invalid email or password. 
                    </div>

                  <?php } ?>


               		<div class="col-md-6">
                		<h1>Sign in to Arena</h1>
                    </div>
                </div>
                
            	<div class="row container center-block">
                    <div class="col-md-6">
                        <form role="form" action="login.php" method="post" id="login">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="e.g. johndoe@gmail.com">
                          </div>
                          <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password">
                          </div>
                          <input type="hidden" name="action" value="login">

                          <p>Forgot your password? <a href="forgot_password.php">Click here</a></p>
      
                          <button type="submit" class="btn btn-primary btn-lg">Login</button>
                        </form>
                        
                        <br>
                        <p>New to Arena? <a href="register.php">Register Now!</a></p>
                        
                    </div>
                    <div class="col-md-6">
                        <h3><i class="fa fa-cubes"></i> Easy to use</h3>
                        <p>Aenean molestie non velit a mattis. Sed sollicitudin risus nisi, ut volutpat magna rhoncus quis. 
                        In commodo vel tellus scelerisque posuere></p>
                        
                        <h3><i class="fa fa-usd"></i> Membership Cash</h3>
                        <p>Aenean molestie non velit a mattis. Sed sollicitudin risus nisi, ut volutpat magna rhoncus quis. 
                        In commodo vel tellus scelerisque posuere</p>
                    </div>
            
                </div>
                <div class="clearfix"></div>
            </div>   
            <!-- Features -->     
            
            <div class="footer">
            	<div class="row container center-block">
                   
                   <div class="footer_link">
                   	<a href="">Contact Support</a> - <a href="">About  Arena</a>
                   </div>
                   
                </div>
       
            </div>             
            
        
        </div><!--content-->
      
     <!-- http://jqueryvalidation.org/ -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="js/jquery.validate.js"></script>
  <script>
    $( "#login" ).validate({
      rules: {
          email: {
            required: true,
            email: true
          },
          password: {
            required: true,
            minlength: 6
          }
      }
    });
  </script>

   </body>
</html>
