<?php
include('system/config.php');
include('controller/customer.php');

if(isset($_POST['action']) == 'register') {
	
	$add_customer = add_customer($_POST);
	
	if ($add_customer == 1) {
		header('Location: '.SITE_URL.'login.php?status=success');
	} 
}
?>
<!DOCTYPE html>
<html>
   <head>
    <title>Arena 88</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale1">
    
    <link rel="stylesheet" href="css/style.css" media="all">
    <link rel="stylesheet" href="css/bootstrap.css" media="all">
    <link rel="stylesheet" href="css/responsive.css" media="all">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"><!-- API http://fortawesome.github.io/Font-Awesome/ -->
	

   </head>
   <body>
   
   		<div class="header">
                <div class="col-xs-10 col-xs-offset-1">
                
                	<div id="logo">
                    	<img src="images/logo.png">
                    </div>
                    
                    <div class="member pull-right">
                      <a href="login.php">Member</a>
                    </div>
                    <div id="menu" class="pull-right">
                      <ul class="nav nav-pills">
                          <li><a href="index.php">Home</a></li>
                          <li><a href="about.php">About</a></li>
                          <li><a href="announcement.php">Announcement</a></li>
                          <li><a href="rss.php">RSS</a></li>
                          <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                    
                </div>
              <div class="clearfix"></div>  
    	</div>
        
        
        <div id="content"><!--content-->
            <!-- Features -->
            <div class="register">
            
            	<div class="row container center-block">

				<!-- Warning -->			
            	 <?php if (isset($add_customer) && ($add_customer == 2)) { ?>

                    <div class="alert alert-warning">
                      <strong>Warning!</strong> Please make sure you have completed all required fields.
                    </div>

                  <?php } ?>

                  <?php if (isset($add_customer) && ($add_customer == 0)) { ?>

                    <div class="alert alert-warning">
                      <strong>Oh snap!</strong> For some reason we failed to register your account, please try again.
                    </div>

                  <?php } ?>

                  <?php if (isset($add_customer) && ($add_customer == 3)) { ?>

                    <div class="alert alert-warning">
                      <strong>Opps!</strong> This email <em><?php echo $_POST['customer_email'] ?></em> already exist, please use different email.
                    </div>

                  <?php } ?>

                  <?php if (isset($add_customer) && ($add_customer == 4)) { ?>

                    <div class="alert alert-warning">
                      <strong>Oh snap!</strong> Please enter again your password.
                    </div>

                  <?php } ?>


               		<div class="col-md-6">
                		<h1>Register your account today</h1>
                    </div>
                </div>
                
            	<div class="row container center-block">
                    <div class="col-md-6">
					<form role="form" action="register.php" method="post" id="register">
						<div class="form-group">
							<label for="customer_name">Name</label>
							<input type="text" class="form-control" id="customer_name" name="customer_name" placeholder="e.g. John Doe" 
							value="<?php echo (isset($_POST['customer_name'])? $_POST['customer_name']:'') ?>">
						</div>
						<div class="form-group">
							<label for="customer_email">Email address</label>
							<input type="email" class="form-control" id="customer_email" name="customer_email" placeholder="e.g. johndoe@gmail.com" value="<?php echo (isset($_POST['customer_email'])? $_POST['customer_email']:'') ?>" required>
						</div>
						<div class="form-group">
							<label for="gender">Gender</label>
							<select name="customer_gender" id="gender" class="form-control">
								<option value="Male">Male</option>
								<option value="Female">Female</option>
							</select>
						</div>
						<div class="form-group">
							<label for="tel">Telephone</label>
							<input type="text" class="form-control" id="tel" name="customer_tel" placeholder="e.g. 0127777555" value="<?php echo (isset($_POST['customer_tel'])? $_POST['customer_tel']:'') ?>"  required>
						</div>
						<div class="form-group">
							<label for="dob_day">Day of Birth</label>
							<div class="clearfix"></div>
							<select name="dob_day" id="dob_day" class="form-control dob_input pull-left">
								<option value="01">1</option>
								<option value="02">2</option>
								<option value="03">3</option>
								<option value="04">4</option>
								<option value="05">5</option>
								<option value="06">6</option>
								<option value="07">7</option>
								<option value="08">8</option>
								<option value="09">9</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
								<option value="24">24</option>
								<option value="25">25</option>
								<option value="26">26</option>
								<option value="27">27</option>
								<option value="28">28</option>
								<option value="29">29</option>
								<option value="30">30</option>
								<option value="31">31</option>
							</select>
							<select name="dob_month" id="dob_month" class="form-control dob_input pull-left">
								<option value="01">Jan</option>
								<option value="02">Feb</option>
								<option value="03">Mar</option>
								<option value="04">Apr</option>
								<option value="05">May</option>
								<option value="06">Jun</option>
								<option value="07">Jul</option>
								<option value="08">Aug</option>
								<option value="09">Sep</option>
								<option value="10">Oct</option>
								<option value="11">Nov</option>
								<option value="12">Dec</option>
							</select>
							<select name="dob_year" id="dob_year" class="form-control dob_input pull-left">
								<?php for($year=1940;$year<2013;$year++) {
									echo '<option value="' . $year . '">' . $year . '</option>';
								}
								?>
							</select>
							<div class="clearfix"></div>
						</div>
						<div class="form-group">
							<label for="street">Street</label>
							<input type="text" class="form-control" id="street" name="customer_address_street" placeholder="e.g. 12-A Batu Unjur" value="<?php echo (isset($_POST['customer_address_street'])? $_POST['customer_address_street']:'') ?>" >
						</div>
						<div class="form-group">
							<label for="city">City</label>
							<input type="text" class="form-control" id="city" name="customer_address_city" placeholder="e.g. Klang" value="<?php echo (isset($_POST['customer_address_city'])? $_POST['customer_address_city']:'') ?>" >
						</div>
						<div class="form-group">
							<label for="poscode">Poscode</label>
							<input type="tel" class="form-control" id="poscode" name="customer_address_poscode" placeholder="e.g. 41000" value="<?php echo (isset($_POST['customer_address_poscode'])? $_POST['customer_address_poscode']:'') ?>" >
						</div>
						<div class="form-group">
							<label for="poscode">States</label>
							<select name="customer_address_state" class="form-control">
								<option value="Johor">Johor</option>
								<option value="Kedah">Kedah</option>
								<option value="Kelantan">Kelantan</option>
								<option value="Labuan">Labuan</option>
								<option value="Melaka">Melaka</option>
								<option value="Negeri Sembilan">Negeri Sembilan</option>
								<option value="Pahang">Pahang</option>
								<option value="Perak">Perak</option>
								<option value="Perlis">Perlis</option>
								<option value="Pulau Pinang">Pulau Pinang</option>
								<option value="Sabah">Sabah</option>
								<option value="Sarawak">Sarawak</option>
								<option value="Selangor">Selangor</option>
								<option value="Terengganu">Terengganu</option>
								<option value="Wilayah Persekutuan">Wilayah Persekutuan</option>
							</select>
						</div>
						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" class="form-control" id="password" name="customer_password" placeholder="Your password" required><br>
							<input type="password" class="form-control" id="customer_password_confirm" name="customer_password_confirm" placeholder='Type your password again to confirm' required>
						</div>
						<input type="hidden" name="action" value="register">
						
						<button type="submit" class="btn btn-primary btn-lg">Submit</button>
					</form>
                        
                    </div>
                    <div class="col-md-6">
                        <h2>Lorem ipsum dolor sit amet adipiscing</h2>
                        
                        <h3><i class="fa fa-cubes"></i> Easy to use</h3>
                        <p>Aenean molestie non velit a mattis. Sed sollicitudin risus nisi, ut volutpat magna rhoncus quis. 
                        In commodo vel tellus scelerisque posuere</p>
                        
                        <h3><i class="fa fa-usd"></i> Membership Cash</h3>
                        <p>Aenean molestie non velit a mattis. Sed sollicitudin risus nisi, ut volutpat magna rhoncus quis. 
                        In commodo vel tellus scelerisque posuere</p>

                        <h3><i class="fa fa-calendar"></i> With Advanced Booking System</h3>
                        <p>Aenean molestie non velit a mattis. Sed sollicitudin risus nisi, ut volutpat magna rhoncus quis. 
                        In commodo vel tellus scelerisque posuere</p>
                    </div>
            
                </div>
                <div class="clearfix"></div>
            </div>   
            <!-- Features -->
        
        </div><!--content-->
      
    <!-- http://jqueryvalidation.org/ -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="js/jquery.validate.js"></script>
	<script>
		$( "#register" ).validate({
			rules: {
					customer_name: {
						required: true,
						minlength: 3
					},
					customer_email: {
				      required: true,
				      email: true
				    },
				    customer_tel: {
						required: true,
						minlength: 7
					},
					customer_address_poscode: {
						number: true,
						minlength: 5
					},
					customer_password: {
						required: true,
						minlength: 8
					},
					 customer_password_confirm: {
						equalTo: "#password"
					}
			}
		});
	</script>
 
   </body>
</html>