<?php
include('system/config.php');
is_login();

include('controller/booking.php');
$list_booking = list_booking($sort_data);
?>
<!DOCTYPE html>
<html>
   <head>
    <title>Arena 88</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale1">
    
    <link rel="stylesheet" href="css/style.css" media="all">
    <link rel="stylesheet" href="css/bootstrap.css" media="all">
    <link rel="stylesheet" href="css/responsive.css" media="all">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"><!-- API http://fortawesome.github.io/Font-Awesome/ -->
   </head>
   <body>
   
   		<div class="header">
                <div class="col-xs-10 col-xs-offset-1">
                
                	<div id="logo">
                    	<img src="images/logo.png">
                    </div>
                    
                    <div class="member pull-right">
                      <?php if (isset($_SESSION['front_login'])) { ?>
                          <a href="account.php">Account</a>
                      <?php } else { ?>
                          <a href="login.php">Member</a>
                      <?php } ?>
                    </div>
                    <div id="menu" class="pull-right">
                      <ul class="nav nav-pills">
                          <li><a href="index.php">Home</a></li>
                          <li><a href="about.php">About</a></li>
                          <li><a href="announcement.php">Announcement</a></li>
                          <li><a href="rss.php">RSS</a></li>
                          <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                    
                </div>
              <div class="clearfix"></div>  
    	</div>
        
        
        <div id="content"><!--content-->
            <!-- Features -->
            <div class="page">
            
            	<div class="row container center-block">


                  <?php if (isset($_GET['status']) && ($_GET['status'] == 'success')) { ?>

                    <div class="alert alert-success">
                      <strong>Success!</strong> Your form was successfully submitted.
                    </div>

                  <?php } ?>

                		<h1>Hello, <?php echo $_SESSION['front_customer_name']; ?></h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt augue ligula.</p>
               
                </div>
                
            	<div class="row container center-block">


                  <ul class="list-unstyled account_menu">
                    <li><a href="edit_account.php"><i class="pull-left fa fa-user"></i>Edit Account</a></li>
                    <li><a href="booking_add.php"><i class="pull-left fa fa-thumb-tack"></i>Booking Court</a></li>
                    <li><a href="report.php"><i class="pull-left fa fa-exclamation-triangle"></i>Report</a></li>
                    <li><a href="report_history.php"><i class="pull-left fa fa-history"></i>Report History</a></li>
                    <li><a href="membership.php"><i class="pull-left fa fa-money"></i>Top Up Membership Cash</a></li>
                    <li><a href="controller/logout.php"><i class="pull-left fa fa-power-off"></i>Log Out</a></li>
                  </ul>


                    <div id="order_history">
                    <div class="table_header">Booking History (Last 10 records)<i class="pull-right fa fa-thumb-tack"></i></div>
                      <table class="table table-striped">
                      <thead>
                        <tr>
                          <th width="100">Booking ID</th>
                          <th width="102">Court Name</th>
                          <th width="100">Amount</th>
                          <th width="150">Date Created</th>
                          <th width="100">Action</th>
                        </tr>
                      </thead>
                      <tbody>

                      <?php 
                      if(empty($list_booking)) { ?>
                        <tr>
                          <td colspan="4">No Record Found!</td>
                        </tr>

                      <?php } else {
                      foreach ($list_booking as $data) { ?>

                        <tr>
                          <td><?php echo $data['booking_id'] ?></td>
                          <td><?php echo $data['court_name'] ?></td>
                          <td>RM <?php echo $data['total'] ?></td>
                          <td><?php echo date('h:i A d-m-Y', strtotime($data['date_created'])); ?></td>
                          <td><a href="booking_info.php?id=<?php echo $data['booking_id'] ?>">View</a></td>
                        </tr>

                      <?php } 
                      }
                      ?>  

                      </tbody>    
                      </table>
                    </div>  
            
                </div>
                <div class="clearfix"></div>
            </div>   
            <!-- Features -->     
            
            <div class="footer">
            	<div class="row container center-block">
                   
                   <div class="footer_link">
                   	<a href="">Contact Support</a> - <a href="">About  Arena</a>
                   </div>
                   
                </div>
       
            </div>             
            
        
        </div><!--content-->
      
 
   </body>
</html>
