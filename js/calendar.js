$(document).ready(function() {

	var curpadding = $('#sidebar').css('padding-bottom');
	var curmargin = $('#sidebar').css('margin-bottom');

	curpadding = parseInt(curpadding);
	curmargin = parseInt(curmargin);
	
	// Select Time slot
	$('.slot').click(function() {		

		var check = $(this).hasClass('selected');
				
		if (check) {

			curpadding = curpadding - 200; 
			curmargin = curmargin + 200;

			$('#sidebar').css('padding-bottom', curpadding + 'px');
			$('#sidebar').css('margin-bottom', curmargin + 'px');


			$(this).removeClass('selected');

			$('#sidebar').css('padding-bottom', curpadding + 'px');
			// Total
			var total = $('#result').attr('rel');
			
			if(total) {
				total = parseInt(total) - 14;
			} else {
				total = 0;	
			}
			
			
			$('#total_amount').val(total);
			$('#result').attr('rel', total).html('RM' + total);

			if(total > 0) {
				$('#membership_cash_required').html(' (required at least <b>' + total + '</b> membership cash)');
			} else {
				$('#membership_cash_required').html('');
			}
			
			// Remove Slot
			var slot = $(this).attr('rel');
			$("#" + slot).remove();
			
		} else {

			curpadding = curpadding + 200; 
			curmargin = curmargin - 200;

			$('#sidebar').css('padding-bottom', curpadding + 'px');
			$('#sidebar').css('margin-bottom', curmargin + 'px');

			$(this).addClass('selected');
				
				// Total
				var total = $('#result').attr('rel');
				
				if(total) {
					total = parseInt(total) + 14;
				} else {
					total = 14;	
				}
				
				$('#total_amount').val(total);
				$('#result').attr('rel', total).html('RM' + total);	
				$('#membership_cash_required').html(' (required at least <b>' + total + '</b> membership cash)');

				
				// Get Slot
				var slot = $(this).attr('rel');
				var date_slot = moment.unix(slot).format("DD-MM-YYYY");
				var time_slot_start = moment.unix(slot).format("hh:mm A");
				var time_slot_end = moment.unix(slot).add(1, 'hours').format("hh:mm A");
				
				$('#slot').append('<tr id="'+ slot +'"><td><input type="hidden" name="slot[]" value="'+ slot +'">'+ date_slot +'</td><td>' + time_slot_start + '</td><td>' + time_slot_end + '</td><td>RM14</td></tr>');	
		
		}

		var membership_cash = $('#current_membership_cash_value').val();
		
		// if customer have membership cash we do something here
		if (membership_cash > total) {
			$('#current_membership_cash').html('Current Membership Cash: ' + membership_cash); // show membership_cash
			$('#membership_cash').prop("disabled", false); 
			cash
			$('#membership_cash_value').val(membership_cash);
		} else {
			$('#current_membership_cash').html('Current Membership Cash: ' + membership_cash); // show membership_cash
			$('#membership_cash').prop("disabled", true); 
			$('#membership_cash').prop("checked", false); 
			$('#cash').prop("checked", true); 
			$('#membership_cash_value').val('0');
		}

		// Disable pay button if we dont have total
		if (total > 0) {
			$('#pay').attr('disabled', false);
		} else {
			$('#pay').attr('disabled', true);
		}
		
	});	
	
});