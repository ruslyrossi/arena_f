<!-- learn jquery here https://buckysroom.org/videos.php?cat=32 -->
$(document).ready(function() {
	
	//  Validate form before we submit
	$('form').submit(function () {
		
		// Get the court_id value
		var court_id = $('#court_id').val();
	
		// Check if court_id empty of not
		if (court_id  === '') {
			alert('Please select Court');
			return false;
		}
	});
	
	// Select Court
	$('.court_slot').click(function(event) {
		event.preventDefault();
		
		var week = $('#week').attr('rel'); // 14
		var nav = $('#nav').attr('rel');   // next
		var court_slot = $(this).attr('rel'); // 3
		
		$('#court_id').val(court_slot); // 2
		
		$('.court_slot').removeClass('selected');
		$(this).addClass('selected');

		// Ajax
		$( "#success" ).load("booking_calendar.php?w=" + week + "&nav=" + nav + "&court_id=" + court_slot, function( response, status, xhr ) {
		  if ( status == "error" ) {
			var msg = "Sorry but there was an error: ";
			$( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
		  }
		});
		
	});

	$('input[name="payment_method"]').on('click', function() {

		var payment_method = $(this).val();

		if (payment_method == 'Cash') {
			$('form').attr('action', 'payment_gateway.php');
		} else {
			$('form').attr('action', 'booking_add.php');
		}

	})
	
	
});