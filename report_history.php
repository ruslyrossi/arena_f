<?php
include('system/config.php');
include('controller/report.php');

is_login();

$list_report = list_report();
?>
<!DOCTYPE html>
<html>
   <head>
    <title>Arena 88</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale1">
    
    <link rel="stylesheet" href="css/style.css" media="all">
    <link rel="stylesheet" href="css/bootstrap.css" media="all">
    <link rel="stylesheet" href="css/responsive.css" media="all">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"><!-- API http://fortawesome.github.io/Font-Awesome/ -->
   </head>
   <body>
   
   		<div class="header">
                <div class="col-xs-10 col-xs-offset-1">
                
                	<div id="logo">
                    	<img src="images/logo.png">
                    </div>
                    
                    <div class="member pull-right">
                      <?php if (isset($_SESSION['front_login'])) { ?>
                          <a href="account.php">Account</a>
                      <?php } else { ?>
                          <a href="login.php">Member</a>
                      <?php } ?>
                    </div>
                    <div id="menu" class="pull-right">
                      <ul class="nav nav-pills">
                          <li><a href="index.php">Home</a></li>
                          <li><a href="about.php">About</a></li>
                          <li><a href="announcement.php">Announcement</a></li>
                          <li><a href="rss.php">RSS</a></li>
                          <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                    
                </div>
              <div class="clearfix"></div>  
    	</div>
        
        
        <div id="content"><!--content-->
            <!-- Features -->
            <div class="page">
            
            	<div class="row container center-block">

                <h1>Report History</h1>
                <ol class="breadcrumb">
                  <li> <a href="account.php">My Account</a></li>
                  <li class="active">Report History</li>
                </ol>

              </div>  
                
            	<div class="row container center-block">

                    <div id="order_history">
                    <div class="table_header">Report History<i class="pull-right fa fa-thumb-tack"></i></div>
                      <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#ID</th>
                          <th>Title</th>
                          <th>Status</th>
                          <th>Date</th>
                  
                        </tr>
                      </thead>
                      <tbody>

                      <?php 
                      if(empty($list_report)) { ?>
                        <tr>
                          <td colspan="4">No Result</td>
                        </tr>

                      <?php } else {
                      foreach ($list_report as $data) { ?>

                        <tr>
                          <td><?php echo $data['report_id'] ?></td>
                          <td><?php echo $data['report_title'] ?></td>
                          <td><?php echo $data['status'] ?></td>
                          <td><?php echo date('Y-m-d H:i:s A', strtotime($data['date_created'])) ?></td>
                        </tr>

                      <?php } 
                      }
                      ?> 
     
                      </tbody>    
                      </table>
                    </div>              
            
                </div>
                <div class="clearfix"></div>
            </div>   
            <!-- Features -->     
            
            <div class="footer">
            	<div class="row container center-block">
                   
                   <div class="footer_link">
                   	<a href="">Contact Support</a> - <a href="">About  Arena</a>
                   </div>
                   
                </div>
       
            </div>             
            
        
        </div><!--content-->
      
 
   </body>
</html>
