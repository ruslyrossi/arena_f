<?php
include('system/config.php');
?>
<!DOCTYPE html>
<html>
   <head>
    <title>Arena 88</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale1">
    
    <link rel="stylesheet" href="css/style.css" media="all">
    <link rel="stylesheet" href="css/bootstrap.css" media="all">
    <link rel="stylesheet" href="css/responsive.css" media="all">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700' rel='stylesheet' type='text/css'>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/bootstrap.min.js"></script>
	<script>
	<!-- carousel image slider -->
    $('.carousel').carousel()
    </script> 
   </head>
   <body>
   
   		<div class="header">
                <div class="col-xs-10 col-xs-offset-1 ipad_width">
                
                	<div id="logo">
                    	<img src="images/logo.png">
                    </div>
                    
                    <div class="member pull-right">
                    <?php if (isset($_SESSION['front_login'])) { ?>
                        <a href="account.php">Account</a>
                    <?php } else { ?>
                        <a href="login.php">Member</a>
                    <?php } ?>
                    </div>
                    <div id="menu" class="pull-right">
                    	<ul class="nav nav-pills">
                          <li class="active"><a href="#">Home</a></li>
                          <li><a href="about.php">About</a></li>
                          <li><a href="announcement.php">Announcement</a></li>
                          <li><a href="rss.php">RSS</a></li>
                          <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                    
                </div>
              <div class="clearfix"></div>  
    	</div>       
        
        <!--carousel-->
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
              </ol>
            
              <!-- Wrapper for slides -->
              <div class="carousel-inner">
                <div class="item active">
                  <img src="images/slide-1.jpg" alt="Slide 1">
                </div>
                <div class="item">
                  <img src="images/slide-2.jpg" alt="Slide 1">
                </div>
                <div class="item">
                  <img src="images/slide-1.jpg" alt="Slide 1">
                </div>
              </div>
            
              <!-- Controls -->
              <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
              </a>
            </div>  
        <!--carousel-->   
        
        <div id="content">
            <!-- Features -->
            <div class="features">
            	<div class="row container center-block">
                    <div class="col-md-4">
                        <img src="images/features-1.jpg" class="img-circle center-block">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam nisl nisl, condimentum quis eleifend eget, 
                        ornare vitae enim. Phasellus dui tortor, accumsan eu viverra vel, dignissim vitae nisl. 
                        Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas</p>
                    </div>
                    <div class="col-md-4">
                        <img src="images/features-2.jpg" class="img-circle center-block">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam nisl nisl, condimentum quis eleifend eget, 
                        ornare vitae enim. Phasellus dui tortor, accumsan eu viverra vel, dignissim vitae nisl. 
                        Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas</p>
                    </div>
                    <div class="col-md-4">
                        <img src="images/features-3.jpg" class="img-circle center-block">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam nisl nisl, condimentum quis eleifend eget, 
                        ornare vitae enim. Phasellus dui tortor, accumsan eu viverra vel, dignissim vitae nisl. 
                        Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>   
            <!-- Features -->      
            
            <!-- Booking preview -->
            <div class="booking_preview">
            	<div class="row container center-block">
                	<h1 class="center-block">Online Court Booking</h1>
                    <p>Sed hendrerit venenatis nisi. Morbi nulla magna, euismod posuere semper vel, cursus nec enim. Aliquam at metus eu arcu porta volutpat</p>
                	<img src="images/booking_preview.png" class="center-block">
                </div>
                <div class="clearfix"></div>
            </div>    
            
            <!-- Review -->
            <div class="review">

                <div class="row container center-block">
                    <div class="flex-video widescreen">
                        <iframe src="//player.vimeo.com/video/55090522?title=0&amp;byline=0&amp;portrait=0" class="center-block" width="830" height="523" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>   
            
            <!-- Ready -->
            <div class="ready">

                <div class="row container center-block">
                	<h1>So players are you ready to rock the courts?</h1>
                    
                    <div class="center">
                        <a href="booking_add.php" class="btn btn-primary btn-xlg">Book Now</a>
                    </div>

                </div>
                
            </div>     
        
        </div>
      
 
   </body>
</html>
