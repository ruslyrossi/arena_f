<?php
include('system/config.php');
include('controller/customer.php');

is_login();

$customer_info = customer_info($_SESSION['front_customer_id']);

$state =  array( 'Johor',
         'Kedah',
         'Kelantan',
         'Labuan',
         'Melaka',
         'Negeri Sembilan',
         'Pahang',
         'Perak',
         'Perlis',
         'Pulau Pinang',
         'Sabah',
         'Sarawak',
         'Selangor',
         'Terengganu',
         'Wilayah Persekutuan',
         );
         
if(isset($_POST['action']) == 'update') {
  
  $update_customer = update_customer($_POST);
  
  if ($update_customer == 1) {
    header('Location: '.SITE_URL.'edit_account.php?status=success');
  } else {
    header('Location: '.SITE_URL.'edit_account.php?status=failed');
  }
}        
?>
<!DOCTYPE html>
<html>
   <head>
    <title>Arena 88</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale1">
    
    <link rel="stylesheet" href="css/style.css" media="all">
    <link rel="stylesheet" href="css/bootstrap.css" media="all">
    <link rel="stylesheet" href="css/responsive.css" media="all">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"><!-- API http://fortawesome.github.io/Font-Awesome/ -->
   </head>
   <body>
   
   		<div class="header">
                <div class="col-xs-10 col-xs-offset-1">
                
                	<div id="logo">
                    	<img src="images/logo.png">
                    </div>
                    
                    <div class="member pull-right">
                      <?php if (isset($_SESSION['front_login'])) { ?>
                          <a href="account.php">Account</a>
                      <?php } else { ?>
                          <a href="login.php">Member</a>
                      <?php } ?>
                    </div>
                    <div id="menu" class="pull-right">
                      <ul class="nav nav-pills">
                          <li><a href="index.php">Home</a></li>
                          <li><a href="about.php">About</a></li>
                          <li><a href="announcement.php">Announcement</a></li>
                          <li><a href="rss.php">RSS</a></li>
                          <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                    
                </div>
              <div class="clearfix"></div>  
    	</div>
        
        
        <div id="content"><!--content-->
            <!-- report -->
            <div class="page">
              
              <div class="row container center-block">

                  <?php if (isset($_GET['status']) && ($_GET['status'] == 'success')) { ?>

                    <div class="alert alert-success">
                      <strong>Success!</strong> Your information has been saved.
                    </div>

                  <?php } ?>

                  <?php if (isset($_GET['status']) && ($_GET['status'] == 'failed')) { ?>

                    <div class="alert alert-warning">
                      <strong>Error!</strong> Something went wrong, check your form again. 
                    </div>

                  <?php } ?>
   
                  <h1>Edit Account</h1>

                  <ol class="breadcrumb">
                    <li> <a href="account.php">My Account</a></li>
                    <li class="active">Edit Account</li>
                  </ol>

              </div>
              
              <div class="row container center-block">
                  
                  <div class="table_header">Edit<i class="pull-right fa fa-thumb-tack"></i></div>
                  <form role="form" class="input_form" action="edit_account.php" method="post">
                    <div class="form-group">
                      <label for="customer_name">Name</label>
                      <input type="text" class="form-control" id="customer_name" name="customer_name" value="<?php echo $customer_info['customer_name']; ?>" required>
                    </div>
                    <div class="form-group">
                      <label for="customer_gender">Gender</label>
                        <select name="customer_gender" id="gender" class="form-control">
                          <option value="Male" <?php echo ($customer_info['customer_gender'] == 'Male'? 'selected':''); ?>>Male</option>
                          <option value="Female" <?php echo ($customer_info['customer_gender'] == 'Female'? 'selected':''); ?>>Female</option>
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="customer_tel">Tel</label>
                      <input type="text" class="form-control" id="customer_tel" name="customer_tel" value="<?php echo $customer_info['customer_tel']; ?>" >
                    </div>
                    <div class="form-group">
                      <label for="customer_address_street">Street</label>
                      <input type="text" class="form-control" id="customer_address_street" name="customer_address_street" value="<?php echo $customer_info['customer_address_street']; ?>" >
                    </div>
                    <div class="form-group">
                      <label for="customer_address_city">City</label>
                      <input type="text" class="form-control" id="customer_address_city" name="customer_address_city" value="<?php echo $customer_info['customer_address_city']; ?>" >
                    </div>
                    <div class="form-group">
                      <label for="customer_address_poscode">Poscode</label>
                      <input type="text" class="form-control" id="customer_address_poscode" name="customer_address_poscode" value="<?php echo $customer_info['customer_address_poscode']; ?>" >
                    </div>
                    <div class="form-group">
                      <label for="customer_address_state">States</label>
                      <select name="customer_address_state" class="form-control">
                       <?php foreach ($state as $item) { ?>
                        <option value="<?php echo $item; ?>" <?php echo ($customer_info['customer_address_state'] == $item? 'selected':'') ?>><?php echo $item; ?></option>
                       <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="customer_password">Password</label>
                      <input type="text" class="form-control" id="customer_password" name="customer_password" placeholder="Leave blank if you don't want to change your password" >
                    </div>

                    <input type="hidden" value="add_report" name="action">
                    <button class="btn btn-primary btn-lg" type="submit">Submit</button>
                  </form>
                                    
                </div>
                
              <div class="clearfix"></div>
            </div>
            <!-- report -->     
            
            <div class="footer">
            	<div class="row container center-block">
                   
                   <div class="footer_link">
                   	<a href="">Contact Support</a> - <a href="">About  Arena</a>
                   </div>
                   
                </div>
       
            </div>             
            
        
        </div><!--content-->
      
 
   </body>
</html>

