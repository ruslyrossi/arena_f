<?php
include('system/config.php');
include('controller/announcement.php');

$list_announcement = list_announcement($sort_data);
?>
<!DOCTYPE html>
<html>
   <head>
    <title>Arena 88</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale1">
    
    <link rel="stylesheet" href="css/style.css" media="all">
    <link rel="stylesheet" href="css/bootstrap.css" media="all">
    <link rel="stylesheet" href="css/responsive.css" media="all">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"><!-- API http://fortawesome.github.io/Font-Awesome/ -->

   </head>
   <body>
   
   		<div class="header">
                <div class="col-xs-10 col-xs-offset-1">
                
                	<div id="logo">
                    	<img src="images/logo.png">
                    </div>
                    
                    <div class="member pull-right">
                      <?php if (isset($_SESSION['front_login'])) { ?>
                          <a href="account.php">Account</a>
                      <?php } else { ?>
                          <a href="login.php">Member</a>
                      <?php } ?>
                    </div>
                    <div id="menu" class="pull-right">
                      <ul class="nav nav-pills">
                          <li><a href="index.php">Home</a></li>
                          <li><a href="about.php">About</a></li>
                          <li class="active"><a href="announcement.php">Announcement</a></li>
                          <li><a href="rss.php">RSS</a></li>
                          <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                    
                </div>
              <div class="clearfix"></div>  
    	</div>
        
        
        <div id="content"><!--content-->
            <!-- Features -->
            <div class="page">
            
              <div class="row container center-block">
                <div class="col-md-12">
                  <h1>Announcement</h1>
                </div>
              </div>

              <div class="row container center-block">

                <?php 
                if(empty($list_announcement)) { ?>
                    No announcement for today.
                  
                <?php } else {
                  foreach ($list_announcement as $data) { ?>

                  <div class="col-md-12 post">
                    <p><b><?php echo $data['announcement_title'] ?></b></p>
                    <div class="date"><?php echo date('j, M Y', strtotime($data['date_created'])) ?></div>
                    <p><?php echo excerpt($data['announcement_content'], 40) ?></p>

                    
                    <a href="announcement_page.php?id=<?php echo $data['announcement_id'] ?>" class="btn btn-primary btn-sm">Read More</a>
                  </div>

                <?php } 
                }
                ?> 

                <div class="pagination pull-right">
                  <?php echo $pagination_object ?>
                </div>
                <div class="clearfix"></div>
                                    
              </div>
   
                <div class="clearfix"></div>
            </div>   
            <!-- Features -->     
            
            <div class="footer">
            	<div class="row container center-block">
                   
                   <div class="footer_link">
                   	<a href="">Contact Support</a> - <a href="">About  Arena</a>
                   </div>
                   
                </div>
       
            </div>             
            
        
        </div><!--content-->
      
 
   </body>
</html>
