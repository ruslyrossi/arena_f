<?php
include('system/config.php');
include('controller/contact.php');

if(isset($_POST['action']) == 'enquiry') {
  
  $send_enquiry = send_enquiry($_POST);
  
  if ($send_enquiry == 1) {
    header('Location: '.SITE_URL.'contact.php?status=success');
  } 
}
?>
<!DOCTYPE html>
<html>
   <head>
    <title>Arena 88</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale1">
    
    <link rel="stylesheet" href="css/style.css" media="all">
    <link rel="stylesheet" href="css/bootstrap.css" media="all">
    <link rel="stylesheet" href="css/responsive.css" media="all">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"><!-- API http://fortawesome.github.io/Font-Awesome/ -->
   </head>
   <body>
   
   		<div class="header">
                <div class="col-xs-10 col-xs-offset-1">
                
                	<div id="logo">
                    	<img src="images/logo.png">
                    </div>
                    
                    <div class="member pull-right">
                      <?php if (isset($_SESSION['front_login'])) { ?>
                          <a href="account.php">Account</a>
                      <?php } else { ?>
                          <a href="login.php">Member</a>
                      <?php } ?>
                    </div>
                    <div id="menu" class="pull-right">
                      <ul class="nav nav-pills">
                          <li><a href="index.php">Home</a></li>
                          <li><a href="about.php">About</a></li>
                          <li><a href="announcement.php">Announcement</a></li>
                          <li><a href="rss.php">RSS</a></li>
                          <li class="active"><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                    
                </div>
              <div class="clearfix"></div>  
    	</div>
        
        
        <div id="content"><!--content-->
            <!-- Features -->
            <div class="page">
            
                <div class="row container center-block">

                  <?php if (isset($_GET['status']) && ($_GET['status'] == 'success')) { ?>

                    <div class="alert alert-success">
                      <strong>Success!</strong> We have received your enquiry and will respond to you within 24 hours.
                    </div>

                  <?php } ?>

                  <div class="col-md-12">
                    <h1>Contact</h1>
                  </div>
                </div>


                <div class="row container center-block">
                
                  <div class="col-md-6">
                      
                      <h2>Address</h2>
                      <h3>Your Company Sdn. Bhd. (562158-T)</h3>
                      <p>B2-16-1, 1st Floor, Menlo Park, California 94025</p>
                      
                      <p><b>Tel:</b> +603-3322 7788</br>
                      <b>Fax:</b> +603-3322 7788</br>
                      <b>Email:</b> support@arena.com.my<br>
                      <b>Business Hour:</b> 9 AM - 12 AM</p>

                    </div>
                    
                    <div class="col-md-6">

                      <h2>Enquiry</h2>
                      <form role="form" action="contact.php" method="post">
                        <div class="form-group">
                          <label for="name">Name</label>
                          <input type="text" class="form-control" id="name" name="name" placeholder="e.g. John Doe"
                          value="<?php echo (isset($_POST['name'])? $_POST['name']:'') ?>" required>
                        </div>
                        <div class="form-group">
                          <label for="email">Email address</label>
                          <input type="email" class="form-control" id="email" name="email" placeholder="e.g. johndoe@gmail.com" value="<?php echo (isset($_POST['email'])? $_POST['email']:'') ?>" required>
                        </div>
                        <div class="form-group">
                          <label for="enquiry">Enquiry</label>
                          <textarea name="enquiry" class="form-control" placeholder="Please write your enquiry here"></textarea>
                        </div>
                        <input type="hidden" name="action" value="enquiry">
                        <button type="submit" class="btn btn-primary btn-lg">Submit</button>
                      </form>

                    </div>
                    
                </div>

                <div class="row container center-block">
                
                  <div class="col-md-12">
                      <br>
                      <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.my/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Badminton+Association+of+Malaysia&amp;aq=&amp;sll=2.977434,101.414772&amp;sspn=0.259544,0.41851&amp;t=m&amp;ie=UTF8&amp;hq=Badminton+Association+of+Malaysia&amp;hnear=&amp;ll=3.140795,101.646634&amp;spn=0.032438,0.052314&amp;z=14&amp;iwloc=A&amp;cid=14547933589704021998&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com.my/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Badminton+Association+of+Malaysia&amp;aq=&amp;sll=2.977434,101.414772&amp;sspn=0.259544,0.41851&amp;t=m&amp;ie=UTF8&amp;hq=Badminton+Association+of+Malaysia&amp;hnear=&amp;ll=3.140795,101.646634&amp;spn=0.032438,0.052314&amp;z=14&amp;iwloc=A&amp;cid=14547933589704021998" style="color:#0000FF;text-align:left">View Larger Map</a></small>
                  </div>

                </div>  
                
   
                <div class="clearfix"></div>
            </div>   
            <!-- Features -->     
           
            <div class="footer">
            	<div class="row container center-block">
                   
                   <div class="footer_link">
                   	<a href="">Contact Support</a> - <a href="">About  Arena</a>
                   </div>
                   
                </div>
       
            </div>             
            
        
        </div><!--content-->
      
 
   </body>
</html>
