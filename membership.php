<?php
include('system/config.php');
include('controller/membership.php');
include('controller/customer.php');

is_login();

$customer_info = customer_info($_SESSION['front_customer_id']);

?>
<!DOCTYPE html>
<html>
   <head>
    <title>Arena 88</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale1">
    
    <link rel="stylesheet" href="css/style.css" media="all">
    <link rel="stylesheet" href="css/bootstrap.css" media="all">
    <link rel="stylesheet" href="css/responsive.css" media="all">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"><!-- API http://fortawesome.github.io/Font-Awesome/ -->
   </head>
   <body>
   
      <div class="header">
        <div class="col-xs-10 col-xs-offset-1">
          
          <div id="logo">
            <img src="images/logo.png">
          </div>
          
          <div class="member pull-right">
            <?php if (isset($_SESSION['front_login'])) { ?>
                <a href="account.php">Account</a>
            <?php } else { ?>
                <a href="login.php">Member</a>
            <?php } ?>
          </div>
          <div id="menu" class="pull-right">
            <ul class="nav nav-pills">
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="announcement.php">Announcement</a></li>
                <li><a href="rss.php">RSS</a></li>
                <li><a href="contact.php">Contact</a></li>
              </ul>
          </div>
          
        </div>
        <div class="clearfix"></div>
      </div>
        
        
        <div id="content"><!--content-->
            <!-- report -->
            <div class="page">
              
              <div class="row container center-block">

                  <?php if (isset($_GET['status']) && ($_GET['status'] == 'failed')) { ?>

                    <div class="alert alert-warning">
                      <strong>Error!</strong> Please check again your form. 
                    </div>

                  <?php } ?>
   
                  <h1>Top Up Membership Cash</h1>

                  <ol class="breadcrumb">
                    <li> <a href="account.php">My Account</a></li>
                    <li class="active">Top Up Membership Cash</li>
                  </ol>

              </div>
              
              <div class="row container center-block">
                  
                  <div class="table_header">Top Up Form<i class="pull-right fa fa-thumb-tack"></i></div>

                 
                    <form role="form" class="input_form" action="payment_gateway.php" method="post" class="form-horizontal"> 
                     <div class="col-md-7">
                      <div class="form-group">

                        <label for="inputEmail3" class="col-sm-4 control-label">Your Membership Cash: </label>
                        <div class="col-sm-8">
                          <div class="cash_reload"><?php echo $customer_info['cash_reload']; ?></div>
                        </div>
                        <div class="clearfix"></div>
                      
                        <label for="inputEmail3" class="col-sm-4 control-label">Amount</label>
                        <div class="col-sm-8">
                          <select name="amount" id="amount" class="form-control" style="width:200px">
                            <option value="10">RM 10</option>
                            <option value="20">RM 20</option>
                            <option value="30">RM 30</option>
                            <option value="40">RM 40</option>
                            <option value="50">RM 50</option>
                            <option value="100">RM 100</option>
                          </select>
                        </div>

                      </div>
                      <div class="clearfix"></div>
                      <input type="hidden" value="topup" name="action">
                      <input type="hidden" value="Top Up Membership Cash" name="item">

                      </br>
                      <button class="btn btn-primary btn-lg" type="submit">Top Up</button>
                      </div>
                      <div class="clearfix"></div>
                    </form>                 
                                    
                </div>
                
              <div class="clearfix"></div>
            </div>
            <!-- report -->     
            
            <div class="footer">
            	<div class="row container center-block">
                   
                   <div class="footer_link">
                   	<a href="">Contact Support</a> - <a href="">About  Arena</a>
                   </div>
                   
                </div>
       
            </div>             
            
        
        </div><!--content-->
      
 
   </body>
</html>

