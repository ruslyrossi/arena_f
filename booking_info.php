<?php
include('system/config.php');
is_login();

include('controller/booking.php');
$booking_info = booking_info($_GET['id']);
$list_booking_slot = list_booking_slot($_GET['id']);
?>
<!DOCTYPE html>
<html>
   <head>
    <title>Arena 88</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale1">
    
    <link rel="stylesheet" href="css/style.css" media="all">
    <link rel="stylesheet" href="css/bootstrap.css" media="all">
    <link rel="stylesheet" href="css/responsive.css" media="all">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"><!-- API http://fortawesome.github.io/Font-Awesome/ -->
   </head>
   <body>
   
   		<div class="header">
                <div class="col-xs-10 col-xs-offset-1">
                
                	<div id="logo">
                    	<img src="images/logo.png">
                    </div>
                    
                    <div class="member pull-right">
                      <?php if (isset($_SESSION['front_login'])) { ?>
                          <a href="account.php">Account</a>
                      <?php } else { ?>
                          <a href="login.php">Member</a>
                      <?php } ?>
                    </div>
                    <div id="menu" class="pull-right">
                      <ul class="nav nav-pills">
                          <li><a href="index.php">Home</a></li>
                          <li><a href="about.php">About</a></li>
                          <li><a href="announcement.php">Announcement</a></li>
                          <li><a href="rss.php">RSS</a></li>
                          <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                    
                </div>
              <div class="clearfix"></div>  
    	</div>
        
        
        <div id="content"><!--content-->
            <!-- Features -->
            <div class="page">
            
            	<div class="row container center-block">
              
                <h1>Booking Info</h1>
                <ol class="breadcrumb">
                  <li> <a href="account.php">My Account</a></li>
                  <li class="active">Booking Info</li>
                </ol>
               
                </div>
                
            	<div class="row container center-block">

                <div class="table_header">Slot<i class="pull-right fa fa-thumb-tack"></i></div>
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Date</th><th>Start</th><th>End</th><th>Price</th>
                    </tr>
                  </thead>
                  <tbody id="slot">
                    
                    <?php
                    if(empty($list_booking_slot)) { ?>
                    <tr>
                      <td colspan="4">No Record Found!</td>
                    </tr>
                    
                    <?php } else {
                    foreach ($list_booking_slot as $data) { ?>
                    <tr id="1401634800">
                      <td><?php echo date('d-m-Y', $data['slot_time']) ?></td>
                      <td><?php echo date('h:i A', $data['slot_time']) ?></td>
                      <td><?php echo date('h:i A', $data['slot_time'] + 3600) ?></td>
                      <td>RM14</td>
                    </tr>
                    <?php }
                    }
                    ?>
                    
                  </tbody>
                </table>
          
            
                </div>
                <div class="clearfix"></div>
            </div>   
            <!-- Features -->     
            
            <div class="footer">
            	<div class="row container center-block">
                   
                   <div class="footer_link">
                   	<a href="">Contact Support</a> - <a href="">About  Arena</a>
                   </div>
                   
                </div>
       
            </div>             
            
        
        </div><!--content-->
      
 
   </body>
</html>
