<!-- navigation error -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="js/calendar.js"></script>
<?php
include('system/config.php');
include('controller/booking.php');

function calendar_week_next($loop = 7, $days = 0, $court_id) {
	
	global $db;
	$court_id = (int)$_GET['court_id'];
	
	// Slot time
	$slot_time = array('-',
					  '9.00 AM',
					  '10.00 AM',
					  '11.00 AM',
					  '12.00 PM',
					  '1.00 PM',
					  '2.00 PM',
					  '3.00 PM',
					  '4.00 PM',
					  '5.00 PM',
					  '6.00 PM',
					  '7.00 PM',
					  '8.00 PM',
					  '9.00 PM',
					  '10.00 PM',
					  '11.00 PM',
					  '12.00 AM');
	
	// Loop Slot time in vertical
	foreach ($slot_time as $slot) {
	
		echo '<tr>';
			
			// Loop date in horizontal
			for ($i=0; $i<$loop; $i++){
				
				$day_next = $i + $days;
				
					if ($slot == '-') {
						echo '<td class="thead">';
						
							echo date('j F', strtotime('+'.$day_next.' days'));
							
						echo '</td>';
						
					} else {
						

						$result =  date('Y-m-d', strtotime('+'.$day_next.' days')) . ' '.$slot;
						$date_unix = strtotime($result);
						
 						// Mark slot if time has passed
						if ($slot == '12.00 AM') {
							$slot_microtime = '11.59 PM'; // microtime think 12:00 AM is early than current timestamp
						} else {
							$slot_microtime = $slot;
						}
						
						$current_time = microtime("now");							
						$slot_date = date('Y-m-d', strtotime('+'.$day_next.' days'));
						$slot_time = strtotime($slot_date." ".$slot_microtime);
						
						if ($current_time > $slot_time) {
							$booked = 'passed';
						} else {

							// Mark slot if already being booked
							$result = $db->query("SELECT b.*, bs.* FROM `booking` b 
												  LEFT JOIN `booking_slot` bs ON (b.booking_id=bs.booking_slot_id)
												  WHERE b.court_id = '".$court_id."' AND bs.slot_time = '".$date_unix."'"); // sql statement for SELECT
							
							if ($result->num_rows) { // check if query return record or not
								$booked = 'booked';
							} else {
								$booked = 'slot';
							}

						}
						
						echo '<td class="'.$booked.'" rel="'.$date_unix.'">';
						
							echo $slot;
							
						echo '</td>';
						
					}
			}
		
		echo '</tr>';
	
	}
}

// Weeks
if(isset($_GET['w'])) {			

	if($_GET['w'] == 0) {
		$days = 0;
		$days_prev = '';
		$days_next = $_GET['w'] + 7;
		$days_next_week = 0;
	} else {
		
		if($_GET['w'] >= 7) {
			$days_prev = $_GET['w'] - 7;
			$days_next = $_GET['w'] + 7;
			$days_next_week = $_GET['w'];
		} else {
			$days = $_GET['w'] + 7;	
			$days_prev = $_GET['w'] + 7;
			$days_next = $_GET['w'] + 7;
			$days_next_week = $_GET['w'] + 7;
		}
	}
} else {
	$days = 0;
	$days_prev = 0;
	
	$days_next = 7;
	$days_next_week = 7;
}

?>

<div id="calendar">
<div class="inner_header">
    <p>Select Slot</p>
    <div class="navigation">
        <?php if (isset($_GET['w']) && ($_GET['w'] >= 1)) { ?>
            <a href="booking_add.php?w=<?php echo $days_prev ?>&nav=prev&court_id=<?php echo $_GET['court_id'] ?>">Prev</a>
        <?php } ?>
        <a href="booking_add.php?w=<?php echo $days_next ?>&nav=next&court_id=<?php echo $_GET['court_id'] ?>">Next (+1 Weeks)</a>
    </div>
    <div class="clearfix"></div>
</div>
<table border="0">
    <tr>
        <?php 
        // Navigation
        if(isset($_GET['nav'])) {
            calendar_week_next(7, $days_next_week, $_GET['court_id']);
            
        } else {
            calendar_week_next(7, $days, $_GET['court_id']);	
        }
        ?>
    </tr>
</table>
</div>    

<div id="total">

<table>
    <thead>
        <tr>
            <td>Date</td><td>Start</td><td>End</td><td>Price</td>
        </tr>    
    </thead>
    <tbody id="slot">
    </tbody>
</table>

<div id="result"></div>
</div>     

<!-- Calendar END-->