<?php
// Pagination
	include('system/pagination.php');

	if (isset($_GET['sort'])) {
		$sort = $_GET['sort'];
	} else {
		$sort = 'announcement_id';
	}
	
	if (isset($_GET['order'])) {
		$order = $_GET['order'];
	} else {
		$order = 'ASC';
	}
	
	if (isset($_GET['page'])) {
		$page = $_GET['page'];
	} else {
		$page = 1;
	}
	
	$url = '';
	
	if (isset($_GET['sort'])) {
		$url .= '&sort=' . $_GET['sort'];
	}
	
	if (isset($_GET['order'])) {
		$url .= '&order=' . $_GET['order'];
	}
	
	$sort_data = array('sort'  => $sort,
				  'order' => $order,
				  'start' => ($page - 1) * PAGINATION_MAX_PER_PAGE,
				  'limit' => PAGINATION_MAX_PER_PAGE
				 );
				 
	$information_total = getTotalAnnouncement();		
			
	$pagination = new Pagination();		
	$pagination->total = $information_total;
	$pagination->page = $page;
	$pagination->limit = PAGINATION_MAX_PER_PAGE;
	$pagination->url = 'announcement.php?' . $url . '&page={page}';
	$pagination_object = $pagination->render();
	
// 	Pagination End

function list_announcement($sort_data) {
	
	global $db;
	
	$sql = '';
	
	// Search
	if (isset($_GET['keyword'])) {
		$keyword = escape($_GET['keyword']);
		$sql .= " WHERE a.announcement_title LIKE '%".$keyword."%' OR a.announcement_content LIKE '%".$keyword."%' ";
	} 
	
	
	// pagination
	if (isset($sort_data['sort'])) {
		$sql .= " ORDER BY " . $sort_data['sort'];	
	} else {
		$sql .= " ORDER BY a.announcement_id";	
	}
	
	if (isset($sort_data['order']) && ($sort_data['order'] == 'DESC')) {
		$sql .= " DESC";
	} else {
		$sql .= " ASC";
	}

	if (isset($sort_data['start']) || isset($sort_data['limit'])) {
		if ($sort_data['start'] < 0) {
			$sort_data['start'] = 0;
		}		

		if ($sort_data['limit'] < 1) {
			$sort_data['limit'] = 3;
		}	
	
		$sql .= " LIMIT " . (int)$sort_data['start'] . "," . (int)$sort_data['limit'];
	}	
	
	$result = $db->query("SELECT a.*, ad.admin_username FROM `announcement` a LEFT JOIN `admin` ad ON (a.admin_id = ad.admin_id) ".$sql." "); // sql statement for SELECT
	
	if ($result->num_rows) { // check if query return record or not
		
		$data = array(); // create data as array()
		
		while ($row = $result->fetch_assoc()) { // loop data into array
			$data[] = $row; // set row into array; row contain announcement data
		}
		
	} else {
		$data = '';
	}
	
	return $data; // return result in array format
}

function getTotalAnnouncement() {
	
	global $db;
		
	$result = $db->query("SELECT * FROM `announcement` ORDER BY announcement_id"); // sql statement for SELECT
	
	$data = $result->num_rows;
	
	return $data; // return result in array format
}

function announcement_info($announcement_id) {
	
	global $db;
	
	$result = $db->query("SELECT * FROM `announcement` WHERE announcement_id = '" .(int)$announcement_id. "'"); // sql statement for SELECT
	
	if ($result->num_rows) { // check if query return record or not
		
		$data = $result->fetch_assoc();
	}
	
	return $data; // return result in array format	
}

?>