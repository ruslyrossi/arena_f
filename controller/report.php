<?php

function add_report($data) {
	
	// Access connection
	global $db;
	
	// Sanitize input
	$report_title = escape($data['report_title']);
	$report_description = escape($data['report_description']);
	$customer_id = $_SESSION['front_customer_id'];
	$date_created = date('Y-m-d H:i:s');
	
	// Insert into DB
	$db->query("INSERT INTO `report` (report_title, report_description, customer_id, date_created) 
			VALUES ('".$report_title."', '".$report_description."', '".$customer_id."', '".$date_created."')");
	
	// Check if insert success
	if($db->affected_rows > 0 ) {
		$status = 1;
	} else {
		$status = 0;
	}
	
	return $status;
}

function list_report() {
	
	global $db;
	
	$result = $db->query("SELECT * FROM `report` WHERE customer_id = '" . $_SESSION['front_customer_id'] . "' "); // sql statement for SELECT
	
	if ($result->num_rows) { // check if query return record or not
		
		$data = array(); // create data as array()
		
		while ($row = $result->fetch_assoc()) { // loop data into array
			$data[] = $row; // set row into array; row contain customers data
		}
		
	} else {
		$data = '';
	}
	
	return $data; // return result in array format
	
}

?>