<?php
// Pagination
	include('system/pagination.php');

	if (isset($_GET['sort'])) {
		$sort = $_GET['sort'];
	} else {
		$sort = 'b.booking_id';
	}
	
	if (isset($_GET['order'])) {
		$order = $_GET['order'];
	} else {
		$order = 'ASC';
	}
	
	if (isset($_GET['page'])) {
		$page = $_GET['page'];
	} else {
		$page = 1;
	}
	
	$url = '';
	
	if (isset($_GET['sort'])) {
		$url .= '&sort=' . $_GET['sort'];
	}
	
	if (isset($_GET['order'])) {
		$url .= '&order=' . $_GET['order'];
	}
	
	$sort_data = array('sort'  => $sort,
				  'order' => $order,
				  'start' => ($page - 1) * PAGINATION_MAX_PER_PAGE,
				  'limit' => PAGINATION_MAX_PER_PAGE
				 );
				 
	$information_total = get_total_booking();		
			
	$pagination = new Pagination();		
	$pagination->total = $information_total;
	$pagination->page = $page;
	$pagination->limit = PAGINATION_MAX_PER_PAGE;
	$pagination->url = 'booking.php?' . $url . '&page={page}';
	$pagination_object = $pagination->render();
	
// 	Pagination End

function add_booking_slot($data) {
	
	// Access connection
	global $db;
	
	// Sanitize input
	$customer_id = $_SESSION['front_customer_id'];
	$court_id = escape($data['court_id']);
	$payment_method = escape($data['payment_method']);
	$membership_cash = escape($data['membership_cash']);
	$total = escape($data['total']);
	$slot = $data['slot'];
	$date_created = date('Y-m-d H:i:s');
	$status = 1;
	$admin_id = 1; // 1 = System
	
	// Membership cash
	if ($payment_method == 'Membership Cash') {
		
		if ($membership_cash >= $total) {
			$db->query("INSERT INTO `customers_cash_reload` (customer_id, cash_reload, admin_id, date_created) VALUES ('".$customer_id."', '-".$total."', ".$admin_id.", '".$date_created."')");
			
			// Update cash_reload
			$db->query("UPDATE `customers` SET cash_reload = cash_reload-'".$total."' WHERE customer_id = '".$customer_id."'");
			
			$status = 1;
			
		} else {
			$status = 0;
		}	
		
	}
	
	
	if ($status == 1) {
		
		// Insert into DB
		$db->query("INSERT INTO `booking` (court_id, customer_id, payment_method, total, date_created) 
				VALUES ('".$court_id."', '".$customer_id."', '".$payment_method."', '".$total."', '".$date_created."')");
				
		$booking_id = $db->insert_id;		
		
		foreach ($data['slot'] as $slot_time) {
			
			$db->query("INSERT INTO `booking_slot` (booking_slot_id	, slot_time) 
				VALUES ('".$booking_id."', '".$slot_time."')");
			
		}
		
			// Check if insert success
			if ($db->affected_rows > 0 ) {
				$status = 1;
			} else {
				$status = 0;
			}
	
	}
	

	
	return $status;
}

function list_court() {
	
	global $db;
	
	$result = $db->query("SELECT * FROM `court`"); // sql statement for SELECT
	
	if ($result->num_rows) { // check if query return record or not
		
		$data = array(); // create data as array()
		
		while ($row = $result->fetch_assoc()) { // loop data into array
			$data[] = $row; // set row into array; row contain customers data
		}
		
	} else {
		$data = '';
	}
	
	return $data; // return result in array format
	
}

function list_booking($sort_data) {
	
	global $db;

	$sql = " WHERE c.customer_id = '" . $_SESSION['front_customer_id'] . "' GROUP BY b.booking_id";
		
	$sql .= " ORDER BY b.booking_id DESC LIMIT 10";	

	$result = $db->query("SELECT b.*, c.customer_name, ct.court_name FROM `booking` b 
						 LEFT JOIN `customers` c ON (b.customer_id = c.customer_id) 
						 LEFT JOIN `court` ct ON (b.court_id = ct.court_id) 
						 ". $sql ." "); // sql statement for SELECT
	
	if ($result->num_rows) { // check if query return record or not
		
		$data = array(); // create data as array()
		
		while ($row = $result->fetch_assoc()) { // loop data into array
			$data[] = $row; // set row into array; row contain customers data
		}
		
	} else {
		$data = '';
	}
	
	return $data; // return result in array format
}

function get_total_booking() {
	
	global $db;
		
	$result = $db->query("SELECT b.* FROM `booking` b GROUP BY b.booking_id ORDER BY b.booking_id"); // sql statement for SELECT
	
	$data = $result->num_rows;
	
	return $data; // return result in array format
}

function booking_info($booking_id) {
	
	global $db;
	
	$result = $db->query("SELECT b.*, c.customer_name, ct.court_name FROM `booking` b 
						 LEFT JOIN `customers` c ON (b.customer_id = c.customer_id) 
						 LEFT JOIN `court` ct ON (b.court_id = ct.court_id) 
						 WHERE b.booking_id = '".(int)$booking_id."'"); // sql statement for SELECT
	
	if ($result->num_rows) { // check if query return record or not
		
		$data = $result->fetch_assoc();
	}
	
	return $data; // return result in array format	
}

function list_booking_slot($booking_id) {
	
	global $db;
	
	$result = $db->query("SELECT * FROM `booking_slot` WHERE booking_slot_id = '" .(int)$booking_id. "'"); // sql statement for SELECT
	
	if ($result->num_rows) { // check if query return record or not
		
		$data = array(); // create data as array()
		
		while ($row = $result->fetch_assoc()) { // loop data into array
			$data[] = $row; // set row into array; row contain customers data
		}
		
	} else {
		$data = '';
	}
	
	return $data; // return result in array format
}

function delete_booking($booking_id) {
	
	global $db;
	
	$db->query("DELETE FROM `booking` WHERE booking_id = '" .(int)$booking_id. "'");
	$db->query("DELETE FROM `booking_slot` WHERE booking_slot_id = '" .(int)$booking_id. "'");
	
	// Check if insert success
	if($db->affected_rows > 0 ) {
		$status = 1;
	} else {
		$status = 0;
	}
	
	return $status;
}

?>




