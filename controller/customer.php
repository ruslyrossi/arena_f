
<?php

function add_customer($data) {
	
	// Access connection for database
	global $db;
	
	// Sanitize input
	$customer_name = escape($data['customer_name']);
	$customer_email = escape($data['customer_email']);
	$customer_gender = escape($data['customer_gender']);
	$customer_tel = escape($data['customer_tel']);
	$customer_dob = escape($data['dob_year']) . '-' . escape($data['dob_month']) . '-' . escape($data['dob_day']);
	$customer_address_street = escape($data['customer_address_street']);
	$customer_address_city = escape($data['customer_address_city']);
	$customer_address_poscode = escape($data['customer_address_poscode']);
	$customer_address_state = escape($data['customer_address_state']);
	$customer_password = $data['customer_password'];
	$customer_password_confirm = $data['customer_password_confirm'];
	$customer_hash = md5(SALT.$customer_password);
	$date_created = date('Y-m-d H:i:s');

	// Validate input
	if (empty($customer_name) || empty($customer_email) || empty($customer_gender) || empty($customer_tel) || empty($customer_password)) {
		return 2;
		break; // break and return status
	}

	if ($customer_password != $customer_password_confirm) {
		return 4;
		break; // break and return status
	}

	// Check if email exist
	$result = $db->query("SELECT customer_email FROM `customers` WHERE customer_email='" . $customer_email . "' "); // sql statement for SELECT
	
	if ($result->num_rows > 0) {
		return 3;
		break; // break and return status
	} 

	// Insert into DB
	$db->query("INSERT INTO `customers` (customer_name, customer_email, customer_password, customer_dob, customer_tel, customer_gender, customer_address_street, customer_address_city, customer_address_poscode, customer_address_state, date_created) 
			VALUES ('".$customer_name."', '".$customer_email."', '".$customer_hash."', '".$customer_dob."', '".$customer_tel."', '".$customer_gender."', '".$customer_address_street."', '".$customer_address_city."', '".(int)$customer_address_poscode."', '".$customer_address_state."', '".$date_created."')");
	
	// Check if insert success
	if($db->affected_rows > 0 ) {
		$status = 1;

	} else {
		$status = 0;

	}
	
	return $status;
}


function customer_info($customer_id) {
	
	global $db;
	
	$result = $db->query("SELECT * FROM `customers` WHERE customer_id = '" .(int)$customer_id. "'"); // sql statement for SELECT
	
	if ($result->num_rows) { // check if query return record or not
		
		$data = $result->fetch_assoc();
	}
	
	return $data; // return result in array format	
}

function update_customer($data) {
	
	// Access connection
	global $db;
	
	// Sanitize input
	$customer_id = $_SESSION['front_customer_id'];
	$customer_name = escape($data['customer_name']);
	$customer_tel = escape($data['customer_tel']);
	$customer_gender = escape($data['customer_gender']);
	$customer_address_street = escape($data['customer_address_street']);
	$customer_address_city = escape($data['customer_address_city']);
	$customer_address_poscode = escape($data['customer_address_poscode']);
	$customer_address_state = escape($data['customer_address_state']);
	$customer_password = escape($data['customer_password']);

	$sql = "";
	if($customer_password) {
		$sql = ", customer_password = '".md5(SALT.$customer_password)."'";
	}
	
	// Insert into DB
	$db->query("UPDATE `customers` SET 
	customer_name = '".$customer_name."', 
	customer_tel = '".$customer_tel."', 
	customer_gender = '".$customer_gender."', 
	customer_address_street = '".$customer_address_street."', 
	customer_address_city = '".$customer_address_city."', 
	customer_address_poscode = '".$customer_address_poscode."', 
	customer_address_state = '".$customer_address_state."' ". $sql ." WHERE customer_id = '".$customer_id."'");
	
	// Check if insert success
	if($db->affected_rows > 0 ) {
		$status = 1;
	} else {
		$status = 0;
	}
	
	return $status;
}


function reset_password($data) {

	// Access connection
	global $db;

	// Sanitize input
	$customer_email = escape($data['email']);
	$customer_password = rand(10000000,99999999);
	$customer_hash = md5(SALT.$customer_password);
	
	// Insert into DB
	$db->query("UPDATE `customers` SET 
	customer_password = '".$customer_hash."' WHERE customer_email = '".$customer_email."'");
	
	// Check if insert success
	if($db->affected_rows > 0 ) {
		$status = 1;

		$result = $db->query("SELECT * FROM `customers` WHERE customer_email = '".$customer_email."'"); // sql statement for SELECT

		if ($result->num_rows) { // check if query return record or not
			
			$data = $result->fetch_assoc();

			notifyPassword($data['front_customer_id'], $customer_password);
		}		

	} else {
		$status = 0;
	}
	
	return $status;
}


function notifyPassword($customer_id, $password) {
	
	$customer_info = customer_info($customer_id);
	
	// HTML email template
	$to = $customer_info['customer_email'];
	
	$subject = 'Your new password for Arena88';
	
	$headers = "From: no-reply@arena88.com\r\n";
	$headers .= "Reply-To: no-reply@arena88.com\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

	$message = '<html><body>';
	$message .= "Hello " .$customer_info['customer_name']. ",<br /><br />

				Email: " .$customer_info['customer_email']. "<br />
				Password: " .$password. "<br /><br />
				
				To change back your password, please visit this page: ".SITE_URL."login.php<br /><br />
				
				All the best,<br />
				Arena88
				";
	$message .= "</body></html>";
		
	mail($to, $subject, $message, $headers);
}

?>