<?php

function send_enquiry($data) {
	
	$name = escape($data['name']);
	$email = escape($data['email']);
	$enquiry = escape($data['enquiry']);

	// validate
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	    // invalid emailaddress

	    return 0;
	    break;
	}
	
	// HTML email template
	$to = ADMIN_EMAIL;
	
	$subject = 'Enquiry';
	
	$headers = "From: " . $email . "\r\n";
	$headers .= "Reply-To: " . $email . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

	$message = '<html><body>';
	$message .= "Name: " .$name. "<br />
				 Email: " .$email. "<br />
				 Enquiry: " .$enquiry. "<br /><br />
				";
	$message .= "</body></html>";
		
	mail($to, $subject, $message, $headers);

	return 1;
}

?>