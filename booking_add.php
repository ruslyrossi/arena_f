<?php
include('system/config.php');
include('controller/booking.php');
include('controller/customer.php');

is_login();

$list_court = list_court();
$customer_info = customer_info($_SESSION['front_customer_id']);

if(isset($_POST['action']) == 'add_booking_slot') {
  
  $add_booking_slot = add_booking_slot($_POST);
  
  if ($add_booking_slot == 1) {
    header('Location: '.SITE_URL.'booking_add.php?status=success');
  } else {
    header('Location: '.SITE_URL.'booking_add.php?status=failed');
  }
}
?>
<!DOCTYPE html>
<html>
   <head>
    <title>Arena 88</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale1">
    
    <link rel="stylesheet" href="css/style.css" media="all">
    <link rel="stylesheet" href="css/bootstrap.css" media="all">
    <link rel="stylesheet" href="css/responsive.css" media="all">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.js"></script>
    <script src="js/moment-with-langs.min.js"></script><!-- http://momentjs.com/docs/ -->
    <script src="js/common.js"></script>
    <script>
    $(document).ready(function() {
      // Default: select court 1
      <?php if (isset($_GET['court_id'])) { ?>
        $("<?php echo '#court_'.$_GET['court_id'] ?>").trigger("click");
      <?php } else { ?>
        $("#court_1").trigger("click");
      <?php } ?>
    });
    </script>

   </head>
   <body>

      <div class="header">
                <div class="col-xs-10 col-xs-offset-1">
                
                  <div id="logo">
                      <img src="images/logo.png">
                    </div>
                    
                    <div class="member pull-right">
                      <?php if (isset($_SESSION['front_login'])) { ?>
                          <a href="account.php">Account</a>
                      <?php } else { ?>
                          <a href="login.php">Member</a>
                      <?php } ?>
                    </div>
                    <div id="menu" class="pull-right">
                      <ul class="nav nav-pills">
                          <li><a href="index.php">Home</a></li>
                          <li><a href="about.php">About</a></li>
                          <li><a href="announcement.php">Announcement</a></li>
                          <li><a href="rss.php">RSS</a></li>
                          <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                    
                </div>
              <div class="clearfix"></div>  
      </div>
      
        <div id="content"><!--content-->
            <!-- Features -->
            <div class="page">
            
              <div class="row container center-block">

                  <?php if (isset($_GET['status']) && ($_GET['status'] == 'success')) { ?>

                    <div class="alert alert-success">
                      <strong>Success!</strong> Your booking has been confirmed. Click <a href="account.php">here</a> for detail.
                    </div>

                  <?php } ?>

                  <?php if (isset($_GET['status']) && ($_GET['status'] == 'failed')) { ?>

                    <div class="alert alert-warning">
                      <strong>Error!</strong> Make sure your membership cash is enough.
                    </div>

                  <?php } ?>

                  <h1>Booking</h1>

                  <ol class="breadcrumb">
                    <li> <a href="account.php">My Account</a></li>
                    <li class="active">Booking Court</li>
                  </ol>
               
              </div>

                <div class="row container center-block">

                    <form action="payment_gateway.php" method="post">
                    
                        <div class="table_header">Online Booking<i class="pull-right fa fa-file-o"></i></div>
                
                        <div id="court_panel"><!--court_panel-->
                            <div class="inner_header">
                                <p>Select Court</p>
                            </div>
                            
                            <?php foreach ($list_court as $court) { ?>
                            
                                <div class="court_slot" rel="<?php echo $court['court_id']; ?>" id="court_<?php echo $court['court_id']; ?>">
                                    <?php echo $court['court_name']; ?>
                                </div>
                            
                            <?php } ?>
                        </div><!--court_panel-->
                
                        <!-- Calendar -->
                        <!-- load calendar -->
                        <div id="success"><img src="images/preloader.gif" class="preloader"></div>   
                        <div id="error"></div>  
                        <div id="week" rel="<?php echo (isset($_GET['w'])? $_GET['w']:'0') ?>"></div>
                        <div id="nav" rel="<?php echo (isset($_GET['nav'])? $_GET['nav']:'nav') ?>"></div>   
                        
                        <div id="payment_method">
                            <table>
                                <thead>
                                    <tr>
                                        <td>Payment Method</td>
                                    </tr>    
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                        <p><span id="current_membership_cash"></span></p>
                                        
                                        <label for="cash">
                                          <input type="radio" value="Cash" name="payment_method" id="cash" checked>Cash
                                        </label><br>
                                        
                                        <label for="membership_cash">
                                          <input type="radio" disabled value="Membership Cash" name="payment_method" id="membership_cash">Membership Cash
                                         </label><span id="membership_cash_required"></span>
                                        </td>
                                    </tr>   
                                </tbody>
                            </table>
                        </div>
                
                        <div class="clearfix"></div>
                        <input type="hidden" value="<?php echo $customer_info['cash_reload']; ?>" id="current_membership_cash_value">
                        <input type="hidden" name="membership_cash" value="" id="membership_cash_value">
                        <input type="hidden" name="court_id" value="" id="court_id">
                        <input type="hidden" name="action" value="add_booking_slot">
                        <input type="hidden" name="total" value="" id="total_amount">
                        <input type="hidden" name="item" value="Booking Court Slot" id="item">

                        <br>
                        
                        <input type="submit" class="btn btn-primary btn-lg" value="Pay" id="pay" disabled="disabled">
                        
                    </form>

                 </div>   

            </div>

            <div class="footer">
                <div class="row container center-block">
                   
                   <div class="footer_link">
                    <a href="">Contact Support</a> - <a href="">About  Arena</a>
                   </div>
                   
                </div>
       
            </div>     
            
        </div>                     
      
   </body>
</html>
